package dyl.easycode.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.lingala.zip4j.io.ZipOutputStream;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import dyl.common.exception.CommonException;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.easycode.bean.EasyCode;
import dyl.easycode.bean.Table;
import dyl.easycode.service.EasyCodeServiceImpl;
import freemarker.template.Configuration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/spring/spring-mvc.xml"})
//当然 你可以声明一个事务管理 每个单元测试都进行事务回滚 无论成功与否  
@TransactionConfiguration(defaultRollback = true)  
//记得要在XML文件中声明事务哦~~~我是采用注解的方式  
@Transactional 
public class EasyCodeLocal {
	@Resource
	JdbcTemplateUtil jdbcTemplate;
	@Resource
	private EasyCodeServiceImpl easyCodeServiceImpl;
	@Test
   	public void easyCode() throws Exception {
		EasyCode easyCode = new EasyCode();
		easyCode.setpKey("id");
		easyCode.setName("sys_role");
    	String destPath="D://easyCode//";
    	File f = new File(destPath);
    	if(!f.exists())f.mkdir();
    	String templatePath=getClass().getResource("../template/layui").getFile().toString();
		//组装ftl需要的参数
    	HashMap<String, Object> root = new HashMap<String, Object>();
    	Table t = easyCodeServiceImpl.getColumnList(easyCode);
		root.put("table", t);
		root.put("sysTime",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		//是否包含上传
		//root.put("hasUpload","true");
		root.put("hasEdit","true");
		root.put("hasDelete","true");
		root.put("hasAuth","true");
		String templates[] =new String[]{
				"javaBean-layui.ftl@"+t.getClassName()+".java",
				"javaAction-layui.ftl@"+t.getClassName()+"Action.java",
				"javaServiceImpl-layui.ftl@"+t.getClassName()+"ServiceImpl.java",
				"viewMain-layui.ftl@"+t.getJavaProperty()+"Main.jsp",
				"viewForm-layui.ftl@"+t.getJavaProperty()+"Form.jsp"};
		easyCode.setTemplates(templates);
		Configuration cfg = FreemarkerUtils.getConfiguration(templatePath);
		for (int i = 0; i < easyCode.getTemplates().length; i++) {
			String arr[] = easyCode.getTemplates()[i].split("@");
			FreemarkerUtils.createFile(cfg,arr[0],root,destPath+arr[1]);
		}
   	}
}
