<#macro elOut el >${'${'}${el}${'}'}</#macro>
<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{todo}</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>

	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  class="layui-elem-quote news_search"  id="mainForm" method="post">
				<#list table.baseColumns as c>
				<#if c.selectSql??>
    			<#else>
    			<div class="layui-inline">
			        <label class="layui-form-label">${c.remarks!c.javaProperty}:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="${c.javaProperty}"  placeholder="请输入${c.remarks!c.javaProperty}" class="layui-input">
			        </div>
			    </div>
    			</#if>
				</#list>
				<div class="layui-inline">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="search" onclick="reloadTable('${table.javaProperty}Table');">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
				<#if hasEdit?? && hasEdit == 'true'>
					<#if hasAuth?? && hasAuth == 'true'>
					<dyl:hasPermission kind="ADD" menuId="<@elOut el="menuId"/>">
					</#if>
					<a href="javascript:;" class="layui-btn layui-btn-small" id="add">
						<i class="layui-icon">&#xe608;</i> 添加
					</a>
					<#if hasAuth?? && hasAuth == 'true'>
					</dyl:hasPermission>
					</#if>
				</#if>
				<#if hasDelete?? && hasDelete == 'true'>
					<#if hasAuth?? && hasAuth == 'true'>
					<dyl:hasPermission kind="DELETE" menuId="<@elOut el="menuId"/>">
					</#if>
					<a href="javascript:;" class="layui-btn layui-btn-small" id="delete">
						<i class="layui-icon">&#xe640;</i> 删除
					</a>
					<#if hasAuth?? && hasAuth == 'true'>
					</dyl:hasPermission>
					</#if>
				</#if>
				</div>
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<div class="layui-form news_list" id="${table.javaProperty}Table">
					<table  lay-data="{<@elOut el="layUiTableConfig"/>,url:'${table.javaProperty}!find${table.className}List.do',id:'${table.javaProperty}Table'}" data-anim="layui-anim-up" class="layui-table" >
					  <thead>
					    <tr>
					   	  <th lay-data="{checkbox:true, fixed: true}"></th>
					   	  <#list table.baseColumns as c>
						  <th lay-data="{field:'${c.javaProperty}', width:138}">${c.remarks!c.javaProperty}</th>							
						  </#list>
						  <th lay-data="{fixed: 'right', width:200, align:'center', toolbar: '#toolbar'}">操作</th>
					    </tr>
					  </thead>
					</table>
				</div>
			</fieldset>
		</div>
		 <script type="text/html" id="toolbar">
		    <#if hasEdit?? && hasEdit == 'true'>
	        	<#if hasAuth?? && hasAuth == 'true'>
				<dyl:hasPermission kind="UPDATE" menuId="<@elOut el="menuId"/>">
				</#if>
				<a href="javascript:;" class="layui-btn layui-btn-mini" data-id="{{d.${table.primaryKey.javaProperty}}}" data-opt="edit">
					<i class="layui-icon">&#xe642;</i> 修改
				</a>
				<#if hasAuth?? && hasAuth == 'true'>
				</dyl:hasPermission>
				</#if>
			</#if>	
	     </script>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<script>
			 //table自适应
		    $('#${table.javaProperty}Table .layui-table-view').width($(window).width()-70);
			<#if hasEdit?? && hasEdit == 'true'>
			//添加方法
			$('#add').click(function(){
				var para={
					url:"${table.javaProperty}!${table.javaProperty}Form.do",
					title:"添加",
					btnOK:"保存"
				};
				addOrUpdate(para);//通用新增修改方法
			});
			//修改方法
			$('[data-opt=edit]').live("click",function(){
				var para={
					url:"${table.javaProperty}!${table.javaProperty}Form.do",
					para:"id="+$(this).attr("data-id"),
					title:"修改",
					btnOK:"修改"
				};
				addOrUpdate(para);//通用新增修改方法
			});
			</#if>	
			<#if hasDelete?? && hasDelete == 'true'>
			//删除方法
			$('#delete').live("click",function(){
				var dataIds = getTableIds("${table.javaProperty}Table");
				if(dataIds){
					layer.confirm('确认删除所选择的吗?', function(index){
						getJsonDataByPost("${table.javaProperty}!delete.do","dataIds="+dataIds,function(data){
							if(data.result){
								layer.msg("删除成功!");
								$('#search').click();//查询
							}else{
								l.alert(data.msg);//错误消息弹出
							}
							layer.close(index);//关闭删除确认提示框
						},true); 
					});
				}
			});
		</#if>	
		</script>
	</body>
</html>