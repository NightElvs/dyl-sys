<#macro elOut el >${'${'}${el}${'}'}</#macro>
<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form class="layui-form" action="${'${'}not empty ${table.javaProperty}?'${table.javaProperty}!update.do':'${table.javaProperty}!add.do'${'}'}"  id="saveOrUpdateForm"><!--编辑表单-->
    <input type="hidden"  name="${table.primaryKey.javaProperty}" value="<@elOut el="${table.javaProperty}.${table.primaryKey.javaProperty}"/>" />
<div class="layui-row">
<#list table.baseColumns as c>
	<div class="layui-col-xs6 layui-col-sm6 layui-col-md6">
  	<#if c.javaType =='BigDecimal'>
		<label class="layui-form-label">${c.remarks!c.javaProperty}</label>
		<div class="layui-input-inline">
		  <input type="text"   name="${c.javaProperty}"   lay-verify="number_" placeholder="请输入${c.remarks!c.javaProperty}" value="<@elOut el="${table.javaProperty}.${c.javaProperty}"/>" class="layui-input"  />
		</div>
  <#elseif c.javaType =='Date'>
		<label class="layui-form-label">${c.remarks!c.javaProperty}</label>
		<div class="layui-input-inline">
		  <input type="text" name="${c.javaProperty}" id="${c.javaProperty}"  placeholder="请选择时间" readonly="readonly"  value="<fmt:formatDate value="<@elOut el="${table.javaProperty}.${c.javaProperty}"/>" type='both' />" class="layui-input"  />
    	</div>
    <#else>
		<label class="layui-form-label">${c.remarks!c.javaProperty}</label>
		<div class="layui-input-inline">
		 <input type="text"   name="${c.javaProperty}"   placeholder="请输入${c.remarks!c.javaProperty}" value="<@elOut el="${table.javaProperty}.${c.javaProperty}"/>" class="layui-input"  />
  		</div>
  	</#if>								
	</div>
</#list>
	</div>
  <!--隐藏提交按钮，用于触发表单校验 -->
  <button lay-filter="saveOrUpdate" lay-submit class="layui-hide" />
</form>
<script>
	form.render();//重新渲染表单
	<#list table.baseColumns as c>
	  <#if c.javaType =='Date'>
		layui.laydate.render({
		    elem: '#${c.javaProperty}' 
		    ,format: 'yyyy-MM-dd HH:mm:ss'
		   //,type:'datetime'
		 });
	  </#if>								
	</#list>
	form.on('submit(saveOrUpdate)', function(data){//表单提交方法
 		getJsonByajaxForm("saveOrUpdateForm","",function(data){
			if(data.result){
				layer.closeAll('page'); //关闭信息框
				layer.msg($('.layui-layer-btn0').text()+"成功!");
				$('#search').click();//查询
			}else{
				l.alert(data.msg);//错误消息弹出
			}
		},true); 
		return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。									
	}); 
</script>
