package dyl.sys.bean;

/**
 * 由 EasyCode自动生成
 * @author Dyl
 * 2017-09-28 13:50:28
*/

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

public class SysConfig implements Serializable {
    private static final long serialVersionUID = 1L;
	/*字段说明：
	*对应db字段名:id 类型:DECIMAL(11) 主键 
	*是否可以为空:否
	*/
	
	private  BigDecimal  id;
	
	/*字段说明：键
	*对应db字段名:c_key 类型:VARCHAR(255)  
	*是否可以为空:是
	*/
	private  String type;
	private  String  cKey;
	
	/*字段说明：值
	*对应db字段名:c_value 类型:VARCHAR(255)  
	*是否可以为空:是
	*/
	
	private  String  cValue;
	
	/*字段说明：字段说明
	*对应db字段名:note 类型:VARCHAR(255)  
	*是否可以为空:是
	*/
	
	private  String  note;
	
	/*字段说明：
	*对应db字段名:create_time 类型:TIMESTAMP(19)  
	*是否可以为空:是
	*/
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private  Date  createTime;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
    public BigDecimal getId() {
        return id;
    }
    public void setId(BigDecimal id) {
        this.id = id;
    }
    public String getCKey() {
        return cKey;
    }
    public void setCKey(String cKey) {
        this.cKey = cKey;
    }
    public String getCValue() {
        return cValue;
    }
    public void setCValue(String cValue) {
        this.cValue = cValue;
    }
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
	public String toString() {
		return 
		"id:"+id+"\n"+
		
		"cKey:"+cKey+"\n"+
		
		"cValue:"+cValue+"\n"+
		
		"note:"+note+"\n"+
		
		"createTime:"+createTime+"\n"+
		"";
	}
}
