package dyl.sys.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MenuNav extends SysMenu implements Serializable{
	/**
	 * 系统左侧菜单集合
	 */
	private static final long serialVersionUID = 1L;
	private boolean spread = false;//是否展开
	private List<MenuNav> children = new ArrayList<MenuNav>();//菜单子集合
	public List<MenuNav> getChildren() {
		return children;
	}
	public void setChildren(List<MenuNav> children) {
		this.children = children;
	}
	public void addMenuNav(MenuNav menuNav) {
		this.children.add(menuNav);
	}
	public boolean isSpread() {
		return spread;
	}
	public void setSpread(boolean spread) {
		this.spread = spread;
	}
	
}
