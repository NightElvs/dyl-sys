package dyl.sys.service;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dyl.common.util.DylSqlUtil;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.bean.SysConfig;
/**

 * @author Dyl
 * 2017-09-28 13:50:28
 */
@Service
@Transactional
public class SysConfigServiceImpl {
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	/**
	 * 说明：分页查询表sys_config记录封装成List集合
	 * @return List<SysConfig
	 */
	public List<SysConfig>  findSysConfigList(Page page,SysConfig sysConfig) throws Exception{
		String sql = "select id,c_key,c_value,note,create_time from sys_config t where 1=1";
		List<Object> con = new ArrayList<Object>();
		if(sysConfig.getId()!=null){
			sql+=" and t.id=?";
			con.add(sysConfig.getId());
		}
		if(StringUtils.isNotEmpty(sysConfig.getCKey())){
			sql+=" and t.c_key like ?";
			con.add("%"+sysConfig.getCKey()+"%");
		}
		if(StringUtils.isNotEmpty(sysConfig.getCValue())){
			sql+=" and t.c_value like ?";
			con.add("%"+sysConfig.getCValue()+"%");
		}
		if(StringUtils.isNotEmpty(sysConfig.getNote())){
			sql+=" and t.note like ?";
			con.add("%"+sysConfig.getNote()+"%");
		}
		List<SysConfig> sysConfigList =  jdbcTemplate.queryForListBeanByPage(sql, con, SysConfig.class, page);
		
		return sysConfigList;
	}
	/**
	 * 说明：根据主键查询表sys_config中的一条记录 
	 * @return SysConfig
	 */
	public SysConfig getSysConfig(BigDecimal  id) throws Exception{
		String sql = "select id,c_key,c_value,note,create_time from sys_config where id=?";
		SysConfig sysConfig  =  jdbcTemplate.queryForBean(sql, new Object[]{id},SysConfig.class); 
		return sysConfig;
	}
	/**
	 * 说明：往表sys_config中插入一条记录
	 * @return int >0代表操作成功
	 */
	public int insertSysConfig(SysConfig sysConfig) throws Exception{
		String sql = "insert into sys_config(id,c_key,c_value,note) values("+DylSqlUtil.getnextSeqNextVal()+",?,?,?)";
		int returnVal=jdbcTemplate.update(sql,new Object[]{sysConfig.getCKey(),sysConfig.getCValue(),sysConfig.getNote()});
		return returnVal;
	}
	/**
	 * 说明：根据主键更新表sys_config中的记录
	 * @return int >0代表操作成功
	 */
	public int updateSysConfig(SysConfig sysConfig) throws Exception{
		String sql = "update sys_config t set ";
		List<Object> con = new ArrayList<Object>();
		if(sysConfig.getCKey()!=null){
			sql+="t.c_key=?,";
			con.add(sysConfig.getCKey());
		}
		if(sysConfig.getCValue()!=null){
			sql+="t.c_value=?,";
			con.add(sysConfig.getCValue());
		}
		if(sysConfig.getNote()!=null){
			sql+="t.note=?,";
			con.add(sysConfig.getNote());
		}
		sql=sql.substring(0,sql.length()-1);
		sql+=" where id=?";
		con.add(sysConfig.getId());
		int returnVal=jdbcTemplate.update(sql,con.toArray());
		return returnVal;
	}
	/**
	 * 说明：根据主键删除表sys_config中的记录
	 * @return int >0代表操作成功
	 */
	public int[] deleteSysConfig(String  ids) throws Exception{
		String sql = "delete from  sys_config where id=?";
		String[] idArr  =  ids.split(",");
		List<Object[]> paraList = new ArrayList<Object[]>(); 
		for (int i = 0; i < idArr.length; i++) {
			paraList.add(new Object[]{idArr[i]});
		}
		return jdbcTemplate.batchUpdate(sql,paraList);
	}
}
