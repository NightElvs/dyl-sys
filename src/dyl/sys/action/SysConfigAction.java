package dyl.sys.action;


import java.math.BigDecimal;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import dyl.common.bean.LayPageData;
import dyl.common.bean.R;
import dyl.common.exception.CommonException;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.Annotation.Auth;
import dyl.sys.Annotation.AuthAction;
import dyl.sys.bean.SysConfig;
import dyl.sys.service.SysConfigServiceImpl;
/**
 * 由dyl EasyCode自动生成
 * @author Dyl
 * 2017-09-28 13:50:28
 */
@Controller
public class SysConfigAction extends BaseAction{
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	@Resource
	private SysConfigServiceImpl sysConfigServiceImpl;
	
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 */
	@Auth
	@RequestMapping(value = "/sysConfig!main.do")
	public String main(Page page,SysConfig sysConfig,HttpServletRequest request){
		return "/sys/sysConfig/sysConfigMain";
	}
	
	@Auth
	@ResponseBody
	@RequestMapping(value = "/sysConfig!findSysConfigList.do")
	public LayPageData  findSysConfigList(Page page,SysConfig sysConfig,HttpServletRequest request){
		try {
			return getlayPageData(sysConfigServiceImpl.findSysConfigList(page, sysConfig),page);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	 /**
	 * 说明：添加或修改
	 * @return String
	 */
	@Auth
	@RequestMapping(value = "/sysConfig!sysConfigForm.do")
	public String  sysConfigForm(BigDecimal id,HttpServletRequest request){
		try {
			if(id!=null)request.setAttribute("sysConfig",sysConfigServiceImpl.getSysConfig(id));
		} catch (Exception e){
			throw new CommonException(request, e);
		}
		return "/sys/sysConfig/sysConfigForm";
	}
	/**
	 * 说明：插入sys_config
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.ADD)
	@ResponseBody()
	@RequestMapping(value = "/sysConfig!add.do")
	public R add(SysConfig sysConfig,HttpServletRequest request){
		try {
			//sysConfig.setCreator(getSysUser(request).getId());
			int ret = sysConfigServiceImpl.insertSysConfig(sysConfig);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：更新sys_config
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.UPDATE)
	@ResponseBody()
	@RequestMapping(value = "/sysConfig!update.do")
	public R update(SysConfig sysConfig,HttpServletRequest request){
		try {
			int ret = sysConfigServiceImpl.updateSysConfig(sysConfig);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：根据主键删除表sys_config中的记录
	 * @return int >0代表操作成功
	 */
	@Auth(action=AuthAction.DELETE)
	@ResponseBody()
	@RequestMapping(value = "/sysConfig!delete.do")
	public R deleteSysConfig(String dataIds,HttpServletRequest request){
		try {
			int[] ret = sysConfigServiceImpl.deleteSysConfig(dataIds);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
}
