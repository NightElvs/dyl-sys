package dyl.sys.action;

import java.math.BigDecimal;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import dyl.common.bean.LayPageData;
import dyl.common.bean.R;
import dyl.common.exception.CommonException;
import dyl.common.util.Page;
import dyl.sys.Annotation.Auth;
import dyl.sys.bean.SysQuartz;
import dyl.sys.bean.SysUser;
import dyl.sys.service.SysQuartzServiceImpl;
/**
 * 由dyl EasyCode自动生成
 * @author Dyl
 * 2017-05-25 16:04:28
 */
@Controller
public class SysQuartzAction extends BaseAction{
	@Resource
	private JdbcTemplate jdbcTemplate;
	@Resource
	private SysQuartzServiceImpl sysQuartzServiceImpl;
	
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 */
	@RequestMapping(value = "/sysQuartz!main.do")
	public String main(Page page,SysQuartz sysQuartz,HttpServletRequest request){
		return "/sys/sysQuartz/sysQuartzMain";
	}
	@Auth
	@ResponseBody
	@RequestMapping(value = "/sysUser!findSysQuartzList.do")
	public LayPageData  findSysQuartzList(Page page,SysQuartz sysQuartz,HttpServletRequest request){
		try {
			return getlayPageData(sysQuartzServiceImpl.findSysQuartzList(page, sysQuartz),page);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	 /**
	 * 说明：添加或修改
	 * @return String
	 */
	@RequestMapping(value = "/sysQuartz!sysQuartzForm.do")
	public String  sysQuartzForm(BigDecimal id,HttpServletRequest request){
		try {
			if(id!=null)request.setAttribute("sysQuartz",sysQuartzServiceImpl.getSysQuartz(id));
		} catch (Exception e){
			throw new CommonException(request, e);
		}
		return "/sys/sysQuartz/sysQuartzForm";
	}
	/**
	 * 说明：插入sys_quartz
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/sysQuartz!add.do")
	public R add(SysQuartz sysQuartz,HttpServletRequest request){
		try {
			int ret = sysQuartzServiceImpl.insertSysQuartz(sysQuartz);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：更新sys_quartz
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/sysQuartz!update.do")
	public R update(SysQuartz sysQuartz,HttpServletRequest request){
		try {
			int ret = sysQuartzServiceImpl.updateSysQuartz(sysQuartz);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
	/**
	 * 说明：根据主键删除表sys_quartz中的记录
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/sysQuartz!delete.do")
	public R deleteSysQuartz(String dataIds,HttpServletRequest request){
		try {
			int[] ret = sysQuartzServiceImpl.deleteSysQuartz(dataIds);
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
	}
}
