<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<title>用户管理</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>
	<body>
		<!-- 查询条件 -->
		<form  class="layui-elem-quote news_search" action="sysUser!main.do" id="sysUserSearchForm" method="post">
			<div class="layui-inline">
			    <label class="layui-form-label">用户名:</label>
		       <div class="layui-input-inline">
		       		  <input type="text" name="username"   placeholder="请输入用户名" class="layui-input">
		       </div>
			</div>
			<div class="layui-inline">
				<a href="javascript:;" class="layui-btn layui-btn-small" id="search" onclick="reloadTable('sysUserTable');">
					<i class="layui-icon">&#xe615;</i> 查询
				</a>
				<dyl:hasPermission kind="ADD" menuId="${menuId}">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="add">
						<i class="layui-icon">&#xe608;</i> 添加
					</a>
				</dyl:hasPermission>
			</div>
		</form>
		<!-- 主table -->
		<div class="layui-form news_list">
			<table  lay-data="{${layUiTableConfig},url:'sysUser!findSysUserList.do',id:'sysUserTable'}" data-anim="layui-anim-up" class="layui-table" >
			  <thead>
			    <tr>
			   	  <!-- <th lay-data="{checkbox:true, fixed: true}"></th> -->
				  <th lay-data="{field:'username', width:120}">用户名</th>
				  <th lay-data="{field:'name', width:120}">名称</th>
				  <th lay-data="{field:'email', width:120}">邮箱</th>
				  <th lay-data="{field:'phone', width:120}">电话</th>
				  <th lay-data="{field:'state', width:70,templet:'#stateTemplte'}">状态</th>
				  <th lay-data="{field:'createTime', width:170}">创建时间</th>
				  <th lay-data="{field:'roleNames', width:170}">所属角色</th>
				  <th lay-data="{fixed: 'right', width:200, align:'center', toolbar: '#barDemo'}">操作</th>
			    </tr>
			  </thead>
			</table>
		</div>
	     <script type="text/html" id="stateTemplte">
            {{#  if(d.state ==0){ }}
             	<i class="layui-btn layui-btn-normal layui-btn-mini">正常</i>
            {{#  }else{ }}
             	<i class="layui-btn layui-btn-danger layui-btn-mini">禁用</i>
            {{#  }  }}
	     </script>
		 <script type="text/html" id="barDemo">
			<dyl:hasPermission kind="DELETE" menuId="${menuId}">
            	<a class="layui-btn layui-btn-danger layui-btn-mini" data-opt="delete" data-id="{{d.id}}">删除</a>
			</dyl:hasPermission>
			<dyl:hasPermission kind="UPDATE" menuId="${menuId}">
            	<a class="layui-btn layui-btn-mini" data-opt="edit" data-id="{{d.id}}">编辑</a>
				<a class="layui-btn layui-btn-orange layui-btn-mini" data-opt="rPwd" data-id="{{d.id}}">重置密码</a>
			</dyl:hasPermission>
	     </script>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<script>
			//添加方法
			$('#add').live("click",function(){
				var para={
					url:"sysUser!sysUserForm.do",
					title:"添加用户",
					btnOK:"保存"
				};
				addOrUpdate(para);
			});
			//修改方法
			$('[data-opt=edit]').live("click",function(){
				var para={
					url:"sysUser!sysUserForm.do",
					para:"id="+$(this).attr("data-id"),
					title:"修改用户",
					btnOK:"修改"
				};
				addOrUpdate(para);
			});
			//删除方法
			$('[data-opt=delete]').live("click",function(){
				var id = $(this).attr("data-id");
				layer.confirm('即将删除该用户及用户下面的所有权限，是否继续?', function(index){
					getJsonDataByPost("sysUser!delete.do","id="+id,function(data){
						if(data.result){
							l.msg('删除成功!');
							$('#search').click();
						}else{
							l.msg(data.msg);
						}
						layer.close(index);
					},true); 
				});
			});
			//重置密码
			$('[data-opt=rPwd]').live("click",function(){
				var userId = $(this).attr("data-id");
				layer.confirm('确认重置密码吗?', function(index){
					getJsonDataByPost("sysUser!rPwd.do","id="+userId,function(data){
						if(data.result){
							layer.alert("密码重置成功!"); 
						}else{
							l.alert(data.msg);
						}
						layer.close(index);
					},true); 
				});
			});
		</script>
	</body>
</html>