<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>代码生成器</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>

	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  class="layui-elem-quote news_search" action="easyCode!main.do" id="mainForm" method="post">
    			<div class="layui-inline">
			        <label class="layui-form-label">表/视图名:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="name"  value="${easyCode.name}" placeholder="请输入" class="layui-input">
			        </div>
			    </div>
				<div class="layui-inline">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="search">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
				</div>
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<div class="layui-field-box layui-form">
					<table class="layui-table admin-table">
						<thead>
							<tr>
								<th>表/视图名</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${easyCodeList}" var="o">
								<tr>
									<td>${o.name}</td>
							        <td>
										<a href="javascript:;" class="layui-btn layui-btn-mini" data-id="${o.name}" data-opt="scdm">
											<i class="layui-icon">&#xe642;</i> 生成代码
										</a>
									</td>
							    </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<script>
			//查询方法
			$('#search').click(function(){
				showLoading();//显示等待框
				$('#mainForm').submit();
			});
			//生成代码
			$('[data-opt="scdm"]').click(function(){
				var para={
					url:"easyCode!easyCodeForm.do",
					para:"name="+$(this).attr("data-id"),
					title:"代码生成器",
					btnOK:"生成",
				};
				addOrUpdate(para);
			});
		</script>
	</body>
</html>