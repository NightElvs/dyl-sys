<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form class="layui-form" action="${not empty sysAuthKind?'sysAuthKind!update.do':'sysAuthKind!add.do'}"  id="saveOrUpdateForm"><!--编辑表单-->
    <input type="hidden"  name="id" value="${sysAuthKind.id}" />
	<div class="layui-form-item">
		<label class="layui-form-label">种类名称</label>
		<div class="layui-input-inline">
		  <input type="text" name="name" required  lay-verify="required" placeholder="请输入种类名称" value="${sysAuthKind.name}" class="layui-input"  />
		</div>
    </div>
  <!--隐藏提交按钮，用于触发表单校验 -->
  <button lay-filter="saveOrUpdate" lay-submit class="layui-hide" />
</form>
<script>
	form.render();//重新渲染表单
	form.on('submit(saveOrUpdate)', function(data){//表单提交方法
 		getJsonByajaxForm("saveOrUpdateForm",'',function(data){
			if(data.result){
				layer.closeAll('page'); //关闭信息框
				layer.alert($('.layui-layer-btn0').text()+"成功!",function(){
					$('#mainForm').submit();
				}); 
			}else{
				l.alert(data.msg);//错误消息弹出
			}
		},true); 
		return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。									
	}); 
</script>
