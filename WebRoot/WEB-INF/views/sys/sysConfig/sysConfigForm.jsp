<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form class="layui-form" action="${not empty sysConfig?'sysConfig!update.do':'sysConfig!add.do'}"  id="saveOrUpdateForm"><!--编辑表单-->
    <input type="hidden"  name="id" value="${sysConfig.id}" />
	<div class="layui-form-item">
		<label class="layui-form-label">键</label>
		<div class="layui-input-inline">
		  <input type="text" name="cKey" required  lay-verify="required" placeholder="请输入键" value="${sysConfig.CKey}" class="layui-input"  />
		</div>
    </div>
	<div class="layui-form-item">
		<label class="layui-form-label">值</label>
		<div class="layui-input-inline">
		  <input type="text" name="cValue"  placeholder="请输入值" value="${sysConfig.CValue}" class="layui-input"  />
		</div>
    </div>
	<div class="layui-form-item">
		<label class="layui-form-label">字段说明</label>
		<div class="layui-input-inline">
		  <input type="text" name="note" placeholder="请输入字段说明" value="${sysConfig.note}" class="layui-input"  />
		</div>
    </div>
  <!--隐藏提交按钮，用于触发表单校验 -->
  <button lay-filter="saveOrUpdate" lay-submit class="layui-hide" />
</form>
<script>
	form.render();//重新渲染表单
	form.on('submit(saveOrUpdate)', function(data){//表单提交方法
 		getJsonByajaxForm("saveOrUpdateForm","",function(data){
			if(data.result){
				layer.closeAll('page'); //关闭信息框
				layer.msg($('.layui-layer-btn0').text()+"成功!");
				$('#search').click();//查询
			}else{
				l.alert(data.msg);//错误消息弹出
			}
		},true); 
		return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。									
	}); 
</script>
