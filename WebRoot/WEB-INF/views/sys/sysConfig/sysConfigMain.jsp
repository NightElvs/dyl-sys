<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{todo}</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>

	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  class="layui-elem-quote news_search" action="sysConfig!main.do" id="mainForm" method="post">
    			<div class="layui-inline">
			        <label class="layui-form-label">键:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="cKey"  value="" placeholder="请输入键" class="layui-input">
			        </div>
			    </div>
				<div class="layui-inline">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="search" onclick="reloadTable('sysConfigTable');">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
					<dyl:hasPermission kind="ADD" menuId="${menuId}">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="add">
						<i class="layui-icon">&#xe608;</i> 添加
					</a>
					</dyl:hasPermission>
					<dyl:hasPermission kind="DELETE" menuId="${menuId}">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="delete">
						<i class="layui-icon">&#xe640;</i> 删除
					</a>
					</dyl:hasPermission>
				</div>
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<div class="layui-form news_list">
					<table  lay-data="{${layUiTableConfig},url:'sysConfig!findSysConfigList.do',id:'sysConfigTable'}" data-anim="layui-anim-up" class="layui-table" >
					  <thead>
					    <tr>
					   	  <th lay-data="{checkbox:true, fixed: true}"></th>
						  <th lay-data="{field:'cKey', width:138}">键</th>							
						  <th lay-data="{field:'cValue', width:138}">值</th>							
						  <th lay-data="{field:'note', width:138}">字段说明</th>							
						  <th lay-data="{field:'createTime', width:138}"></th>							
						  <th lay-data="{fixed: 'right', width:200, align:'center', toolbar: '#toolbar'}">操作</th>
					    </tr>
					  </thead>
					</table>
				</div>
			</fieldset>
		</div>
		 <script type="text/html" id="toolbar">
		        <td>
					<dyl:hasPermission kind="UPDATE" menuId="${menuId}">
					<a href="javascript:;" class="layui-btn layui-btn-mini" data-id="{{d.id}}" data-opt="edit">
						<i class="layui-icon">&#xe642;</i> 修改
					</a>
					</dyl:hasPermission>
				</td>
	     </script>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<script>
			//添加方法
			$('#add').click(function(){
				var para={
					url:"sysConfig!sysConfigForm.do",
					title:"添加",
					btnOK:"保存"
				};
				addOrUpdate(para);//通用新增修改方法
			});
			//修改方法
			$('[data-opt=edit]').live("click",function(){
				var para={
					url:"sysConfig!sysConfigForm.do",
					para:"id="+$(this).attr("data-id"),
					title:"修改",
					btnOK:"修改"
				};
				addOrUpdate(para);//通用新增修改方法
			});
			//删除方法
			$('#delete').live("click",function(){
				var dataIds = getTableIds("sysConfigTable");
				if(dataIds){
					layer.confirm('确认删除所选择的吗?', function(index){
						getJsonDataByPost("sysConfig!delete.do","dataIds="+dataIds,function(data){
							if(data.result){
								layer.msg("删除成功!");
								$('#search').click();//查询
							}else{
								l.alert(data.msg);//错误消息弹出
							}
							layer.close(index);//关闭删除确认提示框
						},true); 
					});
				}
			});
		</script>
	</body>
</html>