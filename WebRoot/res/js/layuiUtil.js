/*Layui变量初始化**/
//var $ = layui.jquery;
//var jQuery = layui.jquery;
var l = layui.layer;
var form = layui.form;
var laypage = layui.laypage;
var table = layui.table;
//table全选
form.on('checkbox(allChoose)',function(data){
   var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]');
   child.each(function(index, item){
	   item.checked = data.elem.checked;
   });
  form.render('checkbox');
});
form.verify({
	number_: function(value, item){ //value：表单的值、item：表单的DOM对象
	    if(value!=""&&isNaN(value)){
	      return '只能填写数字';
	    }
	  }
}); 
//通用添加或者保存方法
function addOrUpdate(para){
	getHtmlDataByPost(para.url,para.para,function(form){
		layer.open({
			type: 1,
			title: para.title,
			content: form,
			btn: [para.btnOK, '取消'],
			shade: false,
			//屏幕宽度小于500则弹出满宽
			area: typeof(para.area) == "undefined"?[$(window).width()<500?$(window).width():($(window).width()/2+'px'), ($(window).height()-100)+"px"]:para.area,
			maxmin: true,
			scrollbar:false,
			shade:0.5,//遮罩透明度
			success:function(layero, index){
				/*setTimeout(function(){
					layui.layer.tips('点击此处关闭窗口', '.layui-layer-setwin .layui-layer-close', {
						tips: 3
					});
				},500)*/
			},
			yes:function(index){//点击保存按钮
				if(typeof(para.submit)!="undefined"){
					$(para.submit).click();
				}else{
					$('button[lay-submit]').click();
				}
				//触发表单的提交事件
				
			}
		});
	},true);
}
function getTableIds(tableId,opt){
	//获取所有选择的列
	var data_ids = "";
	$(table.checkStatus(tableId).data).each(function(i,v){
		if(typeof(opt) == "undefined")
		data_ids+=v.id+",";
		else data_ids+=v[opt]+",";
	});
	if(data_ids){
		return removeLastChar(data_ids);
	}else{
		layer.msg("未勾选任何记录!");
		return false;
	}
}
//根据data-opt 获取所选择的数据
function getdataIds(data_opt){
	//获取所有选择的列
	var data_ids = "";
	$('[data-opt='+data_opt+']').each(function(){
		if($(this).next("div").hasClass("layui-form-checked")){
			data_ids+=$(this).attr("data-id")+",";
		}
	});
	if(data_ids!=""){
		return removeLastChar(data_ids);
	}else{
		layer.msg("未勾选任何记录!");
		return false;
	}
}
function getJsonDataByPost(url,para,onLoadSuccess,isShowLoding) {
	if(isShowLoding)showLoading();
	var type = "post";
	var async = true;
	var dataType = "Json";
	var timeout = 10000;
	var data=para;
	
	$.ajax({
		type : type,
		url : url,
		async : async,
		dataType : dataType,
		timeout : timeout,
		data : data,
		success : function(data, statu) {
			if(isShowLoding)hideLoading();
			onLoadSuccess(data); 
		},
		error : function(XMLHttpRequest, textStatus, errorThrown){
			if(isShowLoding)hideLoading();
		}
	});
}
function getJsonByajaxForm(formId,url,onLoadSuccess,isShowLoding){
	if(typeof(url) == "undefined"||url=='')url=$('#'+formId).attr("action");
	if(isShowLoding)showLoading();
	$.ajax({
		type : "post",
		async : true,
		dataType : 'json',
		url : url,
		data: $('#'+formId).serialize(),
		timeout:10000,//0是没有时间显示
		success : function(data){
			if(isShowLoding)hideLoading();
			onLoadSuccess(data);
		},
		error:function(response,opts){
			if(isShowLoding)hideLoading();
			l.alert("系统故障，请联系管理员!");
			return false;
		}
	});
}
function getHTMLByajaxForm(formID,url,onLoadSuccess,isShowLoding){
	if(typeof(url) == "undefined"||url=='')url=$('#'+formId).attr("action");
	if(isShowLoding)showLoading();
	$.ajax({
		type : "post",
		async : true,
		dataType : 'html',
		url : url,
		data: $('#'+formId).serialize(),
		timeout:10000,//0是没有时间显示
		success : function(data){
			if(isShowLoding)hideLoading();
			onLoadSuccess(data);
		},
		error:function(response,opts){
			if(isShowLoding)hideLoading();
			l.alert("系统故障，请联系管理员!");
			return false;
		}
	});
}
/**
 * Ajax发送post请求，返回数据类型为html
 * @param url           请求地址
 * @param parameters    post参数
 * @param onLoadSuccess Ajax请求成功后回调函数
 */
function getHtmlDataByPost(url, parameters, onLoadSuccess,isShowLoding){
	if(isShowLoding)showLoading();
	var type = "POST";
	var async = true;
	var dataType = "html";
	var timeout = 10000;
	var data = parameters;
	
	$.ajax({
		type : type,
		url : url,
		async : async,
		dataType : dataType,
		timeout : timeout,
		data : data,
		success : function(data, statu){
			if(isShowLoding)hideLoading();
			onLoadSuccess(data);
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			hideLoading();
		}
	});
}
//unicode解码
function reconvert(str){ 
	str = str.replace(/(\\u)(\w{4})/gi,function($0){ 
	return (String.fromCharCode(parseInt((escape($0).replace(/(%5Cu)(\w{4})/g,"$2")),16))); 
	});
	
	str = str.replace(/(&#x)(\w{4});/gi,function($0){ 
	return String.fromCharCode(parseInt(escape($0).replace(/(%26%23x)(\w{4})(%3B)/g,"$2"),16)); 
	});
	return str;
}

function openAwidow(url){
	var iWidth = 1200 ;
	var iHeight = 700 ;
	var iTop = ( window . screen . availHeight - 30 - iHeight ) / 2 ;
	var iLeft = ( window . screen . availWidth - 10 - iWidth ) / 2 ;
	window.open (url, "newwindow", "height="+iHeight+", width="+iWidth+", top="+iTop+",left="+iLeft+",toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, status=no");
}
//根据formId获取参数数组
function getFormArr(formId){
	var arr = {};
	$($('#'+formId).serializeArray()).each(function(i,v){
		arr[v.name]=v.value;
	});
	return arr;
}
//重载layUi表格
function reloadTable(tableId,formId){
	//如果tableId为空，则取当前页面默认的第一个table
	if(typeof(formId) == "undefined"){
		formId = $('form').eq(0).attr("id");
	}
	showLoading();
	table.reload(tableId,{
		  where: getFormArr(formId)//获取form的参数
	});
	$('#'+tableId+' .layui-table-view').width($(window).width()-70);
	hideLoading();
}
/**
 * 自动填充表单
 * @param obj:json数据
 */
function fillForm(obj,id){
	  var key,value,tagName,type,arr;
	  for(x in obj){
	    key = x;
	    value = obj[x];
	    $("#"+id+" [name='"+key+"'],#"+id+" [name='"+key+"[]']").each(function(){
	      tagName = $(this)[0].tagName;
	      type = $(this).attr('type');
	      if(tagName=='INPUT'){
	        if(type=='radio'){
	          $(this).attr('checked',$(this).val()==value);
	        }else if(type=='checkbox'){
	          arr = value.split(',');
	          for(var i =0;i<arr.length;i++){
	            if($(this).val()==arr[i]){
	              $(this).attr('checked',true);
	              break;
	            }
	          }
	        }else{
	          $(this).val(value);
	        }
	      }else if(tagName=='SELECT' || tagName=='TEXTAREA'){
	        $(this).val(value);
	      }else if(tagName=='DIV'){
	        $(this).html(value);
	      }
	      
	    });
	  }
}
function getsuffix(file_name){
	var result = file_name.substr(file_name.lastIndexOf("."));
	return result;
}
function removeLastChar(str){
	return str.substring(0,str.length-1);
}
/**
 * 显示等待框
 * @param title
 * @returns
 */
function showLoading(title){
	layer.load();
}
/**
 * 关闭等待框
 * @param title
 * @returns
 */
function hideLoading(){
	//layer.close(layer.index);
	layer.closeAll('loading'); //关闭加载层
	
	
}

function getQueryString(name,url) {
	var str= new Array(); 
	str = url.split('?');
	if(str != null){
		  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
		    var r = str[1].match(reg);
		    if (r != null) {
		    	return unescape(r[2]); 
		    }
	}
    return "";
 }

//百分比转小数
function per2num(per) {
    return per.replace(/([0-9.]+)%/, function (a, b) {return +b / 100;})
}
function copy(text2copy){
		var flashcopier = 'flashcopier'; 
		if(!document.getElementById(flashcopier)){   
			var divholder = document.createElement('div');  
			divholder.id = flashcopier;  
			document.body.appendChild(divholder); 
			} 
		document.getElementById(flashcopier).innerHTML = ''; 
		var divinfo = '<embed src="http://files.jb51.net/demoimg/200910/_clipboard.swf" FlashVars="clipboard='+text2copy+'" width="0" height="0" type="application/x-shockwave-flash"></embed>';//这里是关键   document.getElementById(flashcopier).innerHTML = divinfo; } 
}